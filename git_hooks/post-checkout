#!/bin/env python

import sys
import os
import json
from subprocess import check_output, check_call, CalledProcessError

try:
    # get the folder of current hook, could be from repo or modules repo
    current_folder = os.path.dirname(__file__)

    # read out configurations
    with open(current_folder+'/control.json', 'r') as f:
        params = json.load(f)

except IOError:
        print(f'.git/hooks/control.json does not exist, please check if template folder had been well set by:')
        print(f'   git config --global init.templateDir')
        print(f'you can also manually clone submodules by:')
        print(f'   git submodule update --init --recursive')
        print(f'   or redo clone:')
        print(f'   git clone --recursive <remote repo url>')
        print(f'Final solution: conact your CMOs')
        sys.exit(0)

# automatic update submodules if triggered by clone
# this operation need only one time during the first checkout of main repo after clone

if params["clone"]:

    print(f'\n==> copy hooks to {current_folder}')

    # get the folder hold all hooks
    git_hooks_folder = current_folder[:current_folder.find('.git')] + '/git_hooks/'
    # copy hooks into .git/hooks
    check_call(['cp', '-f', git_hooks_folder+'*', current_folder])

    # update all modules
    with open(current_folder+'/control.json', 'w') as f:
        params["clone"] = False
        json.dump(params, f)

    # don't need submodule update again, during clone, it will checkout each modules after the main repo checkout
    if "modules" in current_folder:
        sys.exit(0)
    
    print(f'\n==> update modules after clone')
    try:
        check_call(['git', 'submodule', 'update', '--init', '--recursive'])
    except CalledProcessError as e:
        print(f'submodule update command failed, please try manually clone submodules with command:')
        print(f'    git submodule update --init --recursive')
        print(f'    or redo clone:')
        print(f'    git clone --recursive <remote repo url>')
        sys.exit(0)

sys.exit(0)
