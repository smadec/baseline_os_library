#!/bin/env python
import sys
import os
from subprocess import check_output, check_call, CalledProcessError
# define directory of binary
folder_bin = 'C:\\Projects\\git_test\\doc_chip1\\'
folder_code = 'C:\\Projects\\git_test\\project3_os_chip1\\'
# if tag is NULL, will use current checkout (HEAD), otherwise use tag to checkout
tag_bin = ''
tag_code = ''

current_folder = os.getcwd()

os.chdir(folder_code)
if tag_code != '':
    print(f'CODE - Checkout {tag_code} of {folder_code}')
    check_call(['git', 'checkout', tag_code])
else:
    current_commit = check_output("git log | grep '(HEAD'", shell=True).strip().decode()
    print(f'CODE - use current HEAD:\n  {current_commit}')
    
os.chdir(folder_bin)
if tag_bin != '':
    print(f'BIN  - Checkout {tag_bin} of {folder_bin}')
    check_call(['git', 'checkout', tag_bin])
else:
    current_commit = check_output("git log | grep HEAD", shell=True).strip().decode()
    print(f'BIN  - use current HEAD:\n  {current_commit}')
    
print(f'Copy binary files into code folders...', end='')
# create folder if not exist
if os.path.isdir(folder_code+'documents/') == False:
    os.makedirs(folder_code+'documents/')
check_call(['cp', '-f', folder_bin+'doc/*', folder_code+'documents/'])

if os.path.isdir(folder_code+'Projects/project3_os_chip1/') == False:
    os.makedirs(folder_code+'Projects/project3_os_chip1/')
check_call(['cp', '-f', folder_bin+'project/*', folder_code+'Projects/project3_os_chip1/'])

if os.path.isdir(folder_code+'Obj/') == False:
    os.makedirs(folder_code+'Obj/')
check_call(['cp', '-f', folder_bin+'obj/*', folder_code+'Obj/'])

if os.path.isdir(folder_code+'validation/RTV/') == False:
    os.makedirs(folder_code+'validation/RTV/')
check_call(['cp', '-f', folder_bin+'validation/*', folder_code+'Validation/RTV/'])
print(f'done!')
# back to original folder
os.chdir(current_folder)
# return OK
sys.exit(0)